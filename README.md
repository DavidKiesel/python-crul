python-crul
===========

# Introduction

A dumb cURL impostor to illustrate Python development.

The `suddenthought.crul` Python distribution package demonstrates how to use
the tools below.

- the [Python](https://docs.python.org/3/) programming language
- the [`unittest`](https://docs.python.org/3/library/unittest.html) package for
  unit testing
- the [PyBuilder](https://pybuilder.io/) build tool
- the [Sphinx](https://www.sphinx-doc.org/) documentation tool

For documentation see [Welcome to python-crul's
documentation!](https://davidkiesel.bitbucket.io/python-crul).
