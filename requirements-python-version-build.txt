##############################################################################
# primary dependencies

pybuilder==0.12.10
flake8==3.8.4
twine==3.2.0

##############################################################################
# other dependencies

bleach==3.2.1
certifi==2020.12.5
cffi==1.14.4
chardet==3.0.4
colorama==0.4.4
cryptography==3.2.1
docutils==0.16
idna==2.10
jeepney==0.6.0
keyring==21.5.0
mccabe==0.6.1
packaging==20.7
pkginfo==1.6.1
pycodestyle==2.6.0
pycparser==2.20
pyflakes==2.2.0
Pygments==2.7.3
pyparsing==2.4.7
readme-renderer==28.0
requests==2.25.0
requests-toolbelt==0.9.1
rfc3986==1.4.0
SecretStorage==3.3.0
six==1.15.0
tqdm==4.54.1
urllib3==1.26.2
webencodings==0.5.1
