Welcome to python-crul's documentation!
=======================================

Contents
========

.. toctree::
   :maxdepth: 2

   user_guide/index
   developer_guide/index
   api
   unit_test_api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
