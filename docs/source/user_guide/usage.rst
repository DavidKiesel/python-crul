Usage
=====

.. code-block:: bash

   $ crul --help
   usage: crul [-h] [--output OUTPUT] [--headers HEADERS] URL

   A dumb cURL impostor.

   positional arguments:
     URL                   URL

   optional arguments:
     -h, --help            show this help message and exit
     --output OUTPUT, -o OUTPUT
                           output; default: '-' (standard output)
     --headers HEADERS, -H HEADERS
                           headers in JSON format; default: '{}'

   Examples:

       crul 'https://example.com'

       crul \
           --output OUTPUT \
           --headers '{"KEY": "VALUE"}' \
           'https://example.com'
